<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Attendee_Passes_on_payment_completed</fullName>
        <ccEmails>ron@linvio.com</ccEmails>
        <description>Send Attendee Passes on payment completed</description>
        <protected>false</protected>
        <recipients>
            <field>pymt__Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Linvio_Events/Event_Passes_Payment</template>
    </alerts>
    <rules>
        <fullName>Email Attendee Passes</fullName>
        <actions>
            <name>Send_Attendee_Passes_on_payment_completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pymt__PaymentX__c.pymt__OnPaymentCompleted_Trigger__c</field>
            <operation>includes</operation>
            <value>evt__EmailPasses</value>
        </criteriaItems>
        <criteriaItems>
            <field>pymt__PaymentX__c.pymt__Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Sends an email with event attendee passes when the payment OPC tags include evt__EmailPasses</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
