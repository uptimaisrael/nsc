<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Event_Calendar</defaultLandingTab>
    <description>Manage conferences, concerts, seminars and other types of events natively in Salesforce.</description>
    <label>Event Management</label>
    <logo>Event_Management/Linvio_Events_Logo.png</logo>
    <tab>Event_Calendar</tab>
    <tab>Special_Event__c</tab>
    <tab>Session__c</tab>
    <tab>pymt__PaymentX__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Dashboard</tab>
    <tab>Attendee__c</tab>
    <tab>Supplier_Activity__c</tab>
    <tab>APTS_Table__c</tab>
</CustomApplication>
