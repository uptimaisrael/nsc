<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Idle Opportunity Alert</label>
    <logo>Stage_Alert/Idle_Opportunity_Alert_Logo.png</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Opportunity</tab>
    <tab>Stage_Alert_Configuration</tab>
    <tab>Supplier__c</tab>
    <tab>Proposals__c</tab>
    <tab>Project_Location_Lists__c</tab>
    <tab>Project_Overview__c</tab>
    <tab>Proposal_Warehouse__c</tab>
    <tab>Supplier_Activity__c</tab>
    <tab>APTS_Table__c</tab>
</CustomApplication>
