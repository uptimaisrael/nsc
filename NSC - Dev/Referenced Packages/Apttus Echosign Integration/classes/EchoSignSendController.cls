/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class EchoSignSendController extends Apttus_Echosign.ControllerConstants {
    global Id agmtId {
        get;
        set;
    }
    global String attachIds {
        get;
        set;
    }
    global echosign_dev1__SIGN_Agreement__c esAgmt {
        get;
        set;
    }
    global Id esAgmtId {
        get;
        set;
    }
    global String esSendUIPage {
        get;
        set;
    }
    global String vfPageName {
        get;
        set;
    }
    global EchoSignSendController() {

    }
    global EchoSignSendController(ApexPages.StandardController stdController) {

    }
}
