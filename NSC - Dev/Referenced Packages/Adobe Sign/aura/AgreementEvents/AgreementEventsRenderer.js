({
    afterRender : function(component, helper) {
        var ret = this.superAfterRender();
        return ret;
    },
    
    render : function(component, helper) {
        var ret = this.superRender();
        return ret;
    },
    
    rerender : function(component, helper) {
        var ret = this.superRerender();
        
        return ret;
    }
    
})