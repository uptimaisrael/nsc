({
    doInit : function(component, event, helper) {
       helper.loadAgreement(component);
	},
    
    navigateBack : function(component, event, helper) {
       helper.navigateBack(component);
	}
    
})