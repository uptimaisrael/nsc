({
    /*afterRender : function(component, helper) {
        var ret = this.superAfterRender();

        
        return ret;
    },
    
    render : function(component, helper) {
        var ret = this.superRender();

        return ret;
    },
    */
    rerender : function(component, helper) {
        var ret = this.superRerender();
         // Added for the small screen UI
        var mq = window.matchMedia( "(max-width: 480px)" );
        if (mq.matches) {
            helper.changeRecipientRowCSSStylingForMobile(component);
        }

        return ret;
    }
    
})