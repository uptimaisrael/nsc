/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/UploadAttachment/v1/*')
global class UploadAttachment {
    global UploadAttachment() {

    }
    @HttpPost
    global static String attachDoc() {
        return null;
    }
}
