<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Know your Salesforce Instance</description>
    <formFactors>Large</formFactors>
    <label>Octopus</label>
    <tab>Octopus</tab>
    <tab>Supplier_Activity__c</tab>
    <tab>APTS_Table__c</tab>
</CustomApplication>
