({
    doInit : function(component, event, helper) {
        console.info('[AgreementActions]: init  Record Id '+component.get("v.recordId"));
        var qId = component.get("v.recordId");
        helper.getNamespacePrefix(component);
        helper.getAgreement(component, qId);
    },
    onChange : function(component, event, helper) {
        console.info('[AgreementActions]: Changed the Record value so updating :'+component.get("v.recordId"));
        var qId = component.get("v.recordId");
        helper.getAgreement(component, qId);
    },
    generateAgreement : function (component, event, helper) {
        console.info('[AgreementActions]: generate Document for Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningGenerateDocument?id=' + aId + '&action=Generate_Document&templateType=Agreement';
        //Under Construction 
        //helper.fireAction(component,event ,helper,pageUrl);
    },
    activateAgreement : function (component, event, helper) {
        console.info('[AgreementActions]: Activate Record Id '+component.get("v.recordId"));
		var aId = component.get("v.recordId");
        var pageUrl = 'AgreementActivate?id=' + aId;
        helper.fireAction(component,event ,helper,pageUrl);
        
    },
    sendForSignature : function (component, event, helper) {
        console.info('[AgreementActions]: Send for ESignature Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'SendEmail?action=Send_To_Other_Party_For_Signatures&id=' + aId;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    agreementRenew : function (component, event, helper) {
        console.info('[AgreementActions]: Renew  Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningAgreementRenew?id=' + aId ;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    agreementTerminate : function (component, event, helper) {
        console.info('[AgreementActions]: Terminate Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningAgreementTerminate?id=' + aId ;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    agreementExpire : function (component, event, helper) {
        console.info('[AgreementActions]: Expire Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningAgreementExpire?id=' + aId ;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    agreementAmend : function (component, event, helper) {
        console.info('[AgreementActions]: Amend Agreement for Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningAgreementAmend?id=' + aId ;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    generateSupportingDocuments: function (component, event, helper) {
        console.info('[AgreementActions]: generate Supporting Document for Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningGenerateDocument?templateType=Supporting_Document&action=Generate_Supporting_Document&id=' + aId ;
        //Under Construction 
        //helper.fireAction(component,event ,helper,pageUrl);
    },
    regenerateAgreement: function (component, event, helper) {
        console.info('[AgreementActions]: Re-Generating for Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningGenerateDocument?templateType=Agreement&action=Regenerate_Agreement&id=' + aId ;
        //Under Construction 
        //helper.fireAction(component,event ,helper,pageUrl);
    },
    sendForReview: function (component, event, helper) {
        console.info('[AgreementActions]: Sending for Review Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'SendEmail?action=Send_To_Other_Party_For_Review&id=' + aId;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    submitRequest: function (component, event, helper) {
        console.info('[AgreementActions]: Submit Request for Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'AgreementSubmitRequest?id=' + aId;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    cancelRequest: function (component, event, helper) {
        console.info('[AgreementActions]: Cancel Request for Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'AgreementCancel?id=' + aId;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    agreementPreview: function (component, event, helper) {
        console.info('[AgreementActions]: Preview Document for Record Id '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'LightningGenerateDocument?id=' + aId + '&action=Preview_Agreement&templateType=Agreement';
        //Under Construction 
        //helper.fireAction(component,event ,helper,pageUrl);
    },
    checkEsignStatus: function (component, event, helper) {
        console.info('[AgreementActions]: Cheking eSignature Status '+component.get("v.recordId"));
        var aId = component.get("v.recordId");
        var pageUrl = 'SendEmail?action=Send_To_Other_Party_For_Signatures&id=' + aId;
        helper.fireAction(component,event ,helper,pageUrl);
    },
    refreshActions :  function (component,event, helper, message ){
        var agreement = component.get("v.agreement");
        console.info('[refreshActions]: Refreshing with :'+agreement);
        if( agreement != null ) {
            helper.setActionVisiblity( component,"checkesignstatusAction","checkEsignFormula" ,agreement);
            helper.setActionVisiblity( component,"sendforsignatureAction","sendforEsignFormula" ,agreement);
            helper.setActionVisiblity( component,"genraresupportingdocumentsAction","generateSDFormula" ,agreement);
            helper.setActionVisiblity( component,"cancelrequestAction","cancelRequestFormula" ,agreement);
            helper.setActionVisiblity( component,"submitrequestAction","submitRequestFormula" ,agreement);
            helper.setActionVisiblity( component,"sendforreviewAction","sendforreviewFormula" ,agreement);
            helper.setActionVisiblity( component,"previewAction","previewlRequestFormula" ,agreement);            
            helper.setActionVisiblity( component,"expireAction","expireFormula" ,agreement);
            helper.setActionVisiblity( component,"terminateAction","terminateFormula" ,agreement);
            helper.setActionVisiblity( component,"renewAction","renewFormula" ,agreement);
            helper.setActionVisiblity( component,"amendAction","amendFormula" ,agreement);
            helper.setActionVisiblity( component,"activateAction","activateFormula" ,agreement);
            helper.setActionVisiblity( component,"regenrateAction","regenrateFormula" ,agreement);
            helper.setActionVisiblity( component,"genrateAction","generateFormula" ,agreement);       }
        
    }
})