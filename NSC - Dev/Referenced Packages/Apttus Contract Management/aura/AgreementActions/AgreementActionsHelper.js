({
    setActionVisiblity :function(component, actionId , formula , agreement) {
        var isVisible = true;
        var formulaName = component.get("v."+formula);
        console.log( "[setActionVisiblity] : Working on Action-"+actionId);
        if( this.isEmpty (formulaName)){
            console.log( "[setActionVisiblity] : formula not mentioned so default Visiblity for -"+actionId);
            isVisible = this.isActionVisible(component,actionId ,agreement );
        }else {
            console.log( "[setActionVisiblity] : formula is  -"+formulaName  + "  Value os "+agreement[formulaName]);
            isVisible = this.evaluateValue (agreement[formulaName]);
            console.log( "[setActionVisiblity] : formula is  -"+formulaName  + "  evaluated to "+isVisible);
        }
        //Display / hide Action based on Visibility 
        var cmpTarget = component.find(actionId);
        
        if( isVisible ){
            $A.util.addClass(cmpTarget, 'visibleaction');    
            $A.util.removeClass(cmpTarget, 'hideaction');
        }else{
            $A.util.removeClass(cmpTarget, 'visibleaction'); 
            $A.util.addClass(cmpTarget, 'hideaction');
        }
        
    },
    isActionVisible :function (component,actionId ,agreement ){
        var isVisible = true  ;
        var status = this.getStatus(agreement);
        var statusCategory = this.getStatusCategory(agreement);
        if( actionId == "generateAction"){
            isVisible = statusCategory == "Request" ;
        } else if( actionId == "checkesignstatusAction" ){
            isVisible = statusCategory == "In Authoring" ||
                statusCategory == "In Signatures" ||
                statusCategory == "In Filing" ; 
        } else if( actionId == "sendforsignatureAction" ){
            isVisible = statusCategory == "In Authoring"  ||
                (statusCategory == "In Signatures" && status == "Ready for Signatures" ) ||
                (statusCategory == "In Signatures" && status == "Signature Declined" ) ; 
        } else if( actionId == "generatesupportingdocumentsAction" ){
            isVisible = true;
        }else if ( actionId == "cancelrequestAction" ){
            isVisible =  (statusCategory == "Request" ||
                          statusCategory == "In Authoring"||
                          statusCategory == "In Signatures" ||
                          statusCategory == "In Filing" ) &&
                ( status != "Cancelled Request");
        }else if ( actionId == "submitrequestAction" ){
            isVisible =	statusCategory == "Request" && (
                status == "Request" ||
                status == "In Amendment"||
                status == "In Renewal"||
                status == "" );
        }else if ( actionId == "sendforreviewAction" ){
            isVisible =	statusCategory == "In Authoring"||
                statusCategory == "In Signatures" ;
        }else if ( actionId == "previewAction" ){
            isVisible = true;
        }else if ( actionId == "expireAction" ){
            isVisible = statusCategory == "In Effect" && !(status =='Being Amended' || 
                                                           status =='Being Renewed' );
        }else if ( actionId == "terminateAction" ){
            isVisible = statusCategory == "In Effect" && !(status =='Being Amended' ||
                                                           status =='Being Renewed' );
        }else if ( actionId == "renewAction" ){
            isVisible = statusCategory == "In Effect" && !(status =='Being Amended' || 
                                                           status =='Being Renewed' );
        }else if ( actionId == "amendAction" ){
            isVisible = statusCategory == "In Effect" && !(status =='Being Amended' || 
                                                           status =='Being Renewed' );
        }else if ( actionId == "activateAction" ){
            var source = this.getSource( agreement);
            isVisible = 	statusCategory == "In Signatures" ||
                statusCategory == "In Filing" ||
                (	statusCategory == "In Authoring" && 
                 source == "Other Party Paper"
                ); 
        }else if ( actionId == "regenerateAction" ){
            isVisible = statusCategory != "Request" ;
        }  
        return isVisible;
    },
    isEmpty : function (val ){
        return (val === undefined || val == null || val.length <= 0) ? true : false;;
        
    },
    getStatusCategory : function(agreement){
        if ( !this.isEmpty (agreement['Status_Category__c']) ){
            return agreement['Status_Category__c'] ;
        }else{
        	console.log( "[getStatusCategory] :Status_Category__c notfound");
            return agreement['Apttus__Status_Category__c'] ;
        }
    },
    getSource : function(agreement){
        if ( !this.isEmpty (agreement['Source__c']) ){
            return agreement['Source__c'] ;
        }else{
            return agreement['Apttus__Source__c'] ;
        }
    },
    getStatus : function(agreement) {
        if ( !this.isEmpty (agreement['Status__c']) ){
            return agreement['Status__c'] ;
        }else{
            return agreement['Apttus__Status__c'] ;
        }
    },
    evaluateValue : function( value ){
        var result = false;
        if( !this.isEmpty(value) ){
            if( isNaN(value) ){
                //not a number //true , false or any any string show hide
                result = value.toUpperCase() == 'TRUE'  || value.toUpperCase() == 'SHOW' ;
            } else {
                //Number 
                result = value == true || parseInt(value) > 0 ;
            }
        }
        
        return result;
    },
    fireAction : function (component, event, helper, pageUrl ) {
        var aId = component.get("v.recordId");
        var namespacePrefix = component.get("v.namespacePrefix");
        var url = '/apex/'+namespacePrefix + pageUrl;
        if( aId != null ){
            $A.get("e.force:navigateToURL").setParams({"url": url }).fire();
        }
    },
    getNamespacePrefix : function(component) {
       
        var action = component.get("c.getCurrentNamespace");
        action.setParams({});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var namespace  = response.getReturnValue() ;
                if(namespace != null ){
                    component.set("v.namespacePrefix", namespace);
                    console.log("Got namespacePrefix Object : "+namespace );
                }
            }
        });$A.enqueueAction(action);
         
    },
    getAgreement : function(component, qId) {
        //logMessage("Getting Agreement for "+qId);
        var action = component.get("c.getAgreement");
        action.setParams({
            "agreementId": qId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var agreement  = response.getReturnValue() ;
                if(agreement != null ){
                    component.set("v.agreement", agreement);
                    console.log("Got Agreement Object : "+agreement );
                    //show hide Agreement actions
                    $A.enqueueAction(component.get("c.refreshActions"));
                    
                }else{
                    
                }
                
            }
        });
        $A.enqueueAction(action);
    }
})