<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Contains tabs for Wizard design, Wizard Component Library and Wizards (runtime).</description>
    <formFactors>Large</formFactors>
    <label>Apttus Contract Wizard</label>
    <logo>ApttusDocuments/ApttusLogo.gif</logo>
    <tab>WizardDesigns</tab>
    <tab>WizardComponentLibrary</tab>
    <tab>Wizards</tab>
    <tab>APTS_Table__c</tab>
</CustomApplication>
