@IsTest
public class Up_AgreementDistributor_Test {
    
    
    private static testMethod void testAgreementCtrl(){
       
        Account testAcc = new Account(
            Name = 'TestAcc',
            BillingCity = 'Albany',
            BillingState = 'NY',
            BillingCountry = 'United States',
            BillingPostalCode = '99999'
        );
        insert testAcc;        
        
        
        //Create Template
        Apttus__APTS_Template__c tem = new Apttus__APTS_Template__c(
        	Name = 'Agency Agreement',
            Apttus__Type__c = 'Agreement'
        );
        insert tem;
        
        //Create Distributors
        Distributor__c disti1 = new Distributor__c(Name = 'Disti1');
        Distributor__c disti2 = new Distributor__c(Name = 'Disti2');
        insert disti1;
        insert disti2;

        //Create Contact
        Contact con1 = new Contact(
        	FirstName = 'Test',
            LastName = 'Test',
            Email = 'x@x.com',
            Account_Profile_Recipient__c = true,
            Distributor__c = disti1.id
        );
        insert con1;

        //Create Contact
        Contact con2 = new Contact(
        	FirstName = 'Test',
            LastName = 'Test',
            Email = 'x@x.com',
            Account_Profile_Recipient__c = true,
            Distributor__c = disti2.id
        );
        insert con2;        
        
        //Create Agreements
        //Id gpoRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Standard GPO Agreement').getRecordTypeId();
        Apttus__APTS_Agreement__c gpo = new Apttus__APTS_Agreement__c(
            Name = 'Standard GPO Agreement',
            Apttus__Account__c = testAcc.id,
            Apttus__Primary_Contact__c = con1.id
        );
        insert gpo;
        
        //Id agencyRecTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Agency Agreement').getRecordTypeId();
        Apttus__APTS_Agreement__c generic = new Apttus__APTS_Agreement__c(
            Name = 'Generic Agency Agreement',
            Apttus__Parent_Agreement__c = gpo.id,
            Apttus__Account__c = testAcc.id,
            Apttus__Primary_Contact__c = con2.id
        );
        insert generic;
        
		//Create Pricing Basis
		APTS_Table__c pb = new APTS_Table__c(
        	APTS_Program_Includes__c = 'Chemicals',
            APTS_Agreement__c = gpo.id,
            APTS_Tier1__c = 1,
            APTS_Tier2__c = 2
        );
        insert pb;
        
      
        PageReference page = Page.Up_AgreementDistributor;
        Test.setCurrentPage(page);
        ApexPages.currentPage().getParameters().put('Id', generic.Id);
        Up_AgreementDistributorController controller = new Up_AgreementDistributorController();
        test.startTest();
		
        controller.doQuickSave();
        controller.doCancel();
        
        controller.distiWrapperRecordList[0].isSelected = True;
        controller.distiWrapperRecordList[1].isSelected = True;
        //controller.distiWrapperRecordList[2].isSelected = True;
        controller.doSave();

        test.stopTest();        

    }
}