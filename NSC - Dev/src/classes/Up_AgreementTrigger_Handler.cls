public with sharing class Up_AgreementTrigger_Handler {

    public void OnBeforeInsert_GetContacts(List<Apttus__APTS_Agreement__c> newObjects){
        
        set<id> distributorIds = new set<id>();
        map<id, string> distributorToMemberEmail = new map<id, string>();
        
        for(Apttus__APTS_Agreement__c agr : newObjects){
            if(agr.APTS_Created_From_VF__c == true && !string.isEmpty(agr.APTS_Distributor__c)){
                distributorIds.add(agr.APTS_Distributor__c);
            }
        }      
        
        //Get Member Contacts for the agreement's distibutor
        list<Contact> conList = new list<Contact>([SELECT Id, Name, Email, Distributor__c
                                                   FROM Contact
                                                   WHERE Account_Profile_Recipient__c = true 
                                                   AND Email != NULL
                                                   AND Distributor__c IN :distributorIds]);
        if(conList.size() > 0){
            for(Contact c : conList){
                string emails = '';
                if(distributorToMemberEmail.containsKey(c.Distributor__c)){
                    emails = distributorToMemberEmail.get(c.Distributor__c);
                    emails = emails + ';' + c.Email;
                }
                else{
                    emails = c.Email;
                }
                distributorToMemberEmail.put(c.Distributor__c, emails);
            }
            
            if(distributorToMemberEmail.size() > 0){
                for(Apttus__APTS_Agreement__c agr : newObjects){
                    if(agr.APTS_Created_From_VF__c == true && !string.isEmpty(agr.APTS_Distributor__c)){
                        if(distributorToMemberEmail.containsKey(agr.APTS_Distributor__c)){
                            system.debug(LoggingLevel.Error, '* Update Agreement: ' + agr.Name);
                            system.debug(LoggingLevel.Error, '* Email Recipients: ' + distributorToMemberEmail.get(agr.APTS_Distributor__c)); 
                        	agr.APTS_Default_Email_Recipients__c = distributorToMemberEmail.get(agr.APTS_Distributor__c); 
                        }
                    }
                }                   
            }
        }
    }

    public void OnAfterInsert_ClonePricingBasis(List<Apttus__APTS_Agreement__c> newObjects){
        
        //GrandParent (GPO) -> Parent Agreement (Generic) -> Child - Agency Agreement
        set<id> parentAgreementIds = new set<id>();
        set<id> grandparentAgreementIds = new set<id>();
        list<Apttus__APTS_Agreement__c> genericAgreementList = new list<Apttus__APTS_Agreement__c>();
        list<APTS_Table__c> insClonedPricingBasis = new list<APTS_Table__c>();
        map<id, id> genericToGPO = new map<id, id>();
        map<id, set<APTS_Table__c>> gparentAgreementToPricingBasis = new map<id, set<APTS_Table__c>>();
        
        for(Apttus__APTS_Agreement__c agr : newObjects){
            if(agr.APTS_Created_From_VF__c == true && !string.isEmpty(agr.Apttus__Parent_Agreement__c)){
                parentAgreementIds.add(agr.Apttus__Parent_Agreement__c);
            }
        }
        
        //Get GPO "grand parent" agreements ids
        if(parentAgreementIds.size() > 0){
            genericAgreementList = [SELECT Id, Apttus__Parent_Agreement__c
                                    FROM Apttus__APTS_Agreement__c
                                    WHERE Id IN:parentAgreementIds];
            
            if(genericAgreementList.size() > 0){
                for (Apttus__APTS_Agreement__c agr : genericAgreementList){
                    grandparentAgreementIds.add(agr.Apttus__Parent_Agreement__c);
                    //Map Generic Agreement Id to GPO Agreement Id.
                    genericToGPO.put(agr.id, agr.Apttus__Parent_Agreement__c);
                }
            }
            
        }
        
        if(grandparentAgreementIds.size() > 0){
            
            //Get Pricing Basis from Parent Agreement
            string soql = Up_Utils.getCreatableFieldsSOQL('APTS_Table__c', 'APTS_Agreement__c IN:grandparentAgreementIds');
            //system.debug(LoggingLevel.ERROR, '* Dynamic SOQL: ' + soql);
            list<APTS_Table__c> pbList = Database.query(soql);
            
            //Map GParent Agreement Id to a Set of Pricing Basis
            if(pbList.size() > 0){
                for(APTS_Table__c pb : pbList){
                    set<APTS_Table__c> pbSet = new set<APTS_Table__c>();
                    if(gparentAgreementToPricingBasis.containsKey(pb.APTS_Agreement__c)){
                        pbSet.addAll(gparentAgreementToPricingBasis.get(pb.APTS_Agreement__c));
                        pbSet.add(pb);
                    }
                    else{
                        pbSet.add(pb);
                    }
                    if(pbSet.size() > 0){
                    	gparentAgreementToPricingBasis.put(pb.APTS_Agreement__c, pbSet);    
                    }
                }
            }
        }
        
        for(Apttus__APTS_Agreement__c agr : newObjects){
            if(agr.APTS_Created_From_VF__c == true && !string.isEmpty(agr.Apttus__Parent_Agreement__c)){
        
                //For each Agreement created from VF - clone Parent Agreement Pricing Basis, then edit agreement reference to the one just created.
                set<APTS_Table__c> pbSet = new set<APTS_Table__c>();
                
                if(gparentAgreementToPricingBasis.containsKey(genericToGPO.get(agr.Apttus__Parent_Agreement__c))){
                    pbSet.addAll(gparentAgreementToPricingBasis.get(genericToGPO.get(agr.Apttus__Parent_Agreement__c)));
                    
                    //system.debug(LoggingLevel.ERROR, '* Agreement has Pricing Basis on his Parent');
                    //system.debug(LoggingLevel.ERROR, '* pbSet: ' + pbSet);
                    for(APTS_Table__c pb : pbSet){
                        APTS_Table__c clonedPB = pb.clone();
                        clonedPB.APTS_Agreement__c = agr.id;
                        insClonedPricingBasis.add(clonedPB);
                    }                    
                }
            }
        }
        
        if(insClonedPricingBasis.size() > 0){
            system.debug(LoggingLevel.ERROR, '* Insert Pricing Basis: ' + insClonedPricingBasis);
            insert insClonedPricingBasis;
        }
    }    

    public void OnAfterInsert_GenerateAndRedirectDoc(List<Apttus__APTS_Agreement__c> newObjects){
        for(Apttus__APTS_Agreement__c agr : newObjects){
            if(agr.APTS_Created_From_VF__c == true){
                system.debug(LoggingLevel.Error, '* Call Generate and Redirect API for Agreement: ' + agr.id);
                Up_GenerateAndRedirectDoc.generateAndRedirectDoc(agr.id, userInfo.getSessionId(), userInfo.getOrganizationId());
            }
        }           
    }
}