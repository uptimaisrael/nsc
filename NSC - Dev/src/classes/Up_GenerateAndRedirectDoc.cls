public with sharing class Up_GenerateAndRedirectDoc{
    
    private static final String docFormat = 'PDF';
    private static final String accessLevel = 'Read/Write';
    private static final Boolean activeTemplate = true;    

    @Future(callout=true)
    public static void generateAndRedirectDoc(Id AgreementId, string SessionId, string OrganizationId){
    //public static PageReference generateAndRedirectDoc(Id AgreementId, string SessionId, string OrganizationId){    
        Id templateId;
        String generatedDocId;
        String attachmentTitle;
        Integer agreementNameLength;  
        List<echosign_dev1__SIGN_Recipients__c> recipientsList = new List<echosign_dev1__SIGN_Recipients__c>();
        List<Attachment> atts = new list<Attachment>();
        
        //Get agreement data        
        List<Apttus__APTS_Agreement__c> agmts = [SELECT Id, Name, Apttus__Requestor__c, Apttus__Status_Category__c, 
                                                 Apttus__Status__c, Apttus__Contract_End_Date__c, RecordType.Name,
                                                 APTS_Default_Email_Recipients__c, APTS_Distributor__r.Name
                                                 FROM Apttus__APTS_Agreement__c 
                                                 WHERE id = :AgreementId LIMIT 1]; 
        if(agmts.size() > 0){
            system.debug(LoggingLevel.Error, '* Generate And Redirect API for: ' + agmts.get(0).Name + ' (' + agmts.get(0).Id + ')');
            
            //Get template for agreement
            List<Apttus__APTS_Template__c> templates = [SELECT Id, Name 
                                                        FROM Apttus__APTS_Template__c 
                                                        WHERE Name = :agmts.get(0).RecordType.Name
                                                        AND Apttus__IsActive__c=: activeTemplate LIMIT 1];
            
            if(templates.size() > 0){
                templateId = templates.get(0).Id;  
                system.debug(LoggingLevel.Error, '* Template Id: ' + templateId);
            }
            else {
                system.debug(Logginglevel.ERROR, '* No Template ' + agmts.get(0).RecordType.Name + ' Found in System.');
            }                 
            
            //Call GenerateDoc API 
            String apiServerURL = System.Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/14.0/' + OrganizationId;
            if(!Test.isRunningTest()){
                generatedDocId = Apttus.MergeWebService.generateDoc(templateId, agreementId, accessLevel, docFormat, SessionId, apiServerURL);
                if(generatedDocId != null){
                    system.debug(LoggingLevel.Error, '* Attachment Generated: ' + generatedDocId);
                }
            }
        }
        
        
        //fetching attachment to populate the agreement name
        if(generatedDocId != null){
            atts = [SELECT id, name, body, parentID  FROM Attachment WHERE id =:generatedDocId limit 1];
            
            if(atts.size() > 0){
                attachmentTitle = atts.get(0).name;
                agreementNameLength = 80;
            }
        }

        //create EchoSignAgreement and pre-populating the message and name
        echosign_dev1__SIGN_Agreement__c echosignAgreement = new echosign_dev1__SIGN_Agreement__c();   
        echosignAgreement.Apttus_Echosign__Apttus_Agreement__c = agmts.get(0).id;
        
        if(!string.isEmpty(attachmentTitle) && !attachmentTitle.contains('signed')) {             
            Integer index = attachmentTitle.lastindexOf('.');    
            if(index != -1) {
                attachmentTitle = attachmentTitle.substring(0, index);
            } 
            
            Integer len = attachmentTitle.length();
            if (len > agreementNameLength)
                len = agreementNameLength;
            echosignAgreement.Name = attachmentTitle.substring(0, len);
        }
        else{
            echosignAgreement.Name = agmts.get(0).name;
            echosignAgreement.Name = agmts.get(0).name;
            echosignAgreement.Name = agmts.get(0).name;
        }
        
        Database.SaveResult resultEchoAgr = Database.insert(echosignAgreement);
        if (resultEchoAgr.isSuccess()) {
            system.debug(Logginglevel.ERROR, '* Created Eechosign Agreement Id: ' + resultEchoAgr.getId());
        }
       
        
        //creating EchoSignRecipients
        list<String> emails = new list<string>();
        if(!string.isEmpty(agmts.get(0).APTS_Default_Email_Recipients__c)){
            emails = agmts.get(0).APTS_Default_Email_Recipients__c.split(';');    
        }
        
        if(emails.size() > 0){
            integer i = 0;
            for(string email : emails){
                echosign_dev1__SIGN_Recipients__c rec = new echosign_dev1__SIGN_Recipients__c();
                rec.echosign_dev1__Agreement__c = echosignAgreement.Id;
                rec.echosign_dev1__Recipient_Type__c = 'Email';
                rec.echosign_dev1__Email_Address__c = email;
                rec.echosign_dev1__Order_Number__c = i;   
                recipientsList.add(rec);
                i++;
            }            
        }

        if(recipientsList.size() > 0){
            Database.SaveResult[] resultEchoRecp = Database.insert(recipientsList);
            for(Database.SaveResult sr : resultEchoRecp){
                if (sr.isSuccess()) {
                    system.debug(Logginglevel.ERROR, '* Created EchoSign Recipients Id: ' + sr.getId());
                }                   
            }
        }

        //Attaching the document to Echosign agreement
        if(atts.size() > 0){
            Attachment attToEchoSignAgreement = atts.get(0).clone();
            attToEchoSignAgreement.parentId = echosignAgreement.id;
            insert attToEchoSignAgreement;
        }
        
        // update echosignAgreement;
        echosignAgreement.echosign_dev1__ReturnURL__c = '/'+ AgreementID;
        echosignAgreement.echosign_dev1__Signature_Flow__c = 'Order Entered';
        echosignAgreement.APTS_Created_From_VF__c = true;
        update echosignAgreement;   
    }

}