public class Up_AgreementDistributorController {
    
    private Id agreementId;
    public string agreementName {get;set;}
    private Apttus__APTS_Agreement__c parentAgreement;
    public List<distiRecordCls> distiWrapperRecordList {get;set;}
    
    //Constructor
    public Up_AgreementDistributorController(){
        List<Distributor__c> distiList = new List<Distributor__c>();
        distiWrapperRecordList = new List<distiRecordCls>();
        agreementId = ApexPages.currentPage().getParameters().get('Id');
        set<id> DistributorIds = new set<id>();
        map<id, set<id>> distibutorToParentAgreements = new map<id, set<id>>();
        
        agreementName = [SELECT Name FROM Apttus__APTS_Agreement__c WHERE Id =:agreementId].Name;
        
        //Get ALL distributors.        
        distiList = [SELECT Id, Name FROM Distributor__c];
        
        //Put all distributors ids into a set.
        if(distiList.size() > 0){
            for(Distributor__c disti : distiList){
                DistributorIds.add(disti.id);
            }
        }
        
        if(DistributorIds.size() > 0){
            id recType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Agency Agreement').getRecordTypeId();
            //Get the existing Agency Agreements for all Distributors.
            list<Apttus__APTS_Agreement__c> existingAgreements = [SELECT Id, Name, Apttus__Parent_Agreement__c, APTS_Distributor__c
                                                                  FROM Apttus__APTS_Agreement__c
                                                                  WHERE RecordTypeId = :recType
                                                                  AND APTS_Distributor__c IN:DistributorIds];
            
            for(Apttus__APTS_Agreement__c agreement : existingAgreements){
                set<id> parentIds = new set<id>();
                if(distibutorToParentAgreements.containsKey(agreement.APTS_Distributor__c)){
                    parentIds.addAll(distibutorToParentAgreements.get(agreement.APTS_Distributor__c));
                    parentIds.add(agreement.Apttus__Parent_Agreement__c);
                }
                else{
                    parentIds.add(agreement.Apttus__Parent_Agreement__c);
                }
                if(parentIds.size() > 0){
                    distibutorToParentAgreements.put(agreement.APTS_Distributor__c, parentIds);    
                }
            }
        }
        
        if(distiList.size() > 0){
            for(Distributor__c disti : distiList){
                distiRecordCls distiCls = new distiRecordCls();
                //check if Agency Agreements already exist for this Distributor for this Parent Agreement.
                set<id> parentIds = new set<id>();
                if(distibutorToParentAgreements.containsKey(disti.id)){
                    parentIds.addAll(distibutorToParentAgreements.get(disti.id));
                }
                if(parentIds.size() > 0){
                    if(parentIds.contains(agreementId)){
                        system.debug(LoggingLevel.Error, '* Distributor ' + disti.Name + ' already has a Agency Agreement for Parent Agreement ' + agreementId);
                    }                    
                    else{
                        distiCls.distiObj = disti;
                        distiWrapperRecordList.add(distiCls);                           
                    }
                }
                else{
                    distiCls.distiObj = disti;
                    distiWrapperRecordList.add(distiCls);                                        
                }
            }
        }	
        //system.debug(LoggingLevel.Error, '* Disti Wrapper List Size: ' + distiWrapperRecordList.size());        
        
    }
    
    //Quick Save
    public PageReference doQuickSave(){
        try {
            Boolean success = createAgencyAgreements(); 
            if(success){
                //PageReference tempPage = ApexPages.currentPage();            
                //tempPage.setRedirect(true);
                //tempPage.getParameters().put('Id', agreementID );
                //system.debug('*****Save: Agreement Id: ' + agreementID);
                return NULL;
                
            }
        }
        catch(Exception ex) {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }        
        return null;
    }    
    
    //Save
    public PageReference doSave() {
        try {
            Boolean success = createAgencyAgreements(); 
            if(success){
                PageReference tempPage = new PageReference('/' + agreementId);           
                tempPage.setRedirect(true);
                //system.debug('*****Save: Agreement Id: ' + agreementID);
                return tempPage;
                
            }
        }
        catch(Exception ex) {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        } 
        return null;
        
    }    
    
    //createAgencyAgreements - Commit
    private Boolean createAgencyAgreements(){
        List<distiRecordCls> distiWrapperRemoveRecords = new list<distiRecordCls>();
        list<Apttus__APTS_Agreement__c> insChildAgreement = new list<Apttus__APTS_Agreement__c>();
        
        Apttus__APTS_Agreement__c parentAgreement = [SELECT Id, Name, Apttus__Account__c, APTS_Opportunity_Owner__c, APTS_Corporate_D_B_A_Name__c,
                                                     APTS_Customer_Sold_To__c, APTS_Corporate_Account_Director__c, APTS_Category_Development__c,
                                                     APTS_Customer_Service_Rep__c, APTS_Customer_Service_Backup__c, APTS_Credit_Rep__c,
                                                     APTS_Integrated_Billing_Rep__c, Apttus__Contract_Start_Date__c, Apttus__Contract_End_Date__c,
                                                     APTS_Implementation_Date__c, APTS_Location_Ship_To__c, APTS_Local_Presence_Salesperson_Required__c,
                                                     APTS_Network_Fee__c, APTS_IT_fee__c, APTS_Customer_Incentive_Rebate__c,
                                                     APTS_Other_Fee_Describe__c, APTS_Customer_Negotiated__c, APTS_Mandatory_Manufacturers__c,
                                                     APTS_Network_Negotiated__c, APTS_Common_cost_contracted__c, APTS_Tier_1_Defenition__c,
                                                     APTS_Tier_2_Defenition__c, APTS_Tier_3_Defenition__c, APTS_Tier_4_Defenition__c,
                                                     APTS_Tier_5_Defenition__c, APTS_Tier_6_Defenition__c, APTS_Tier_7_Defenition__c,
                                                     APTS_Pricing_Basis__c, APTS_Other_Pricing_basis__c, APTS_Pricing_Notes__c,
                                                     APTS_Dispenser_Program__c, APTS_Proprietary_Products__c, APTS_Dead_Stock__c,
                                                     APTS_Slow_Moving_Stock__c, APTS_Inventory_Notes__c, APTS_PO_Required__c,
                                                     APTS_Order_guides_required_from_Network__c, APTS_Back_order_policy__c, APTS_CustomerApprovalNecessaryfor_Order__c,
                                                     APTS_Electronic_Order_Guide_MaintenanceN__c, APTS_Minimum_Order_Fee__c, APTS_Minimum_order_size__c,
                                                     APTS_Minimum_Fee_Amount__c, APTS_Bill_and_Transmit_Mini_order_fee_as__c, APTS_Emergency_Order_Fee_SAP_Nbr__c,
                                                     APTS_Emergency_order_fee_notes__c, APTS_Emergency_Order_Fee_UPC__c, APTS_Order_fee_SAP_Nbr__c,
                                                     APTS_Large_Order_Discount__c, APTS_LOD_order_size__c, APTS_LOD_Fee_Amount__c,
                                                     APTS_LOD_fee_UPC__c, APTS_LOD_fee_SAP_Nbr__c,  APTS_Restocking_Fee__c,
                                                     APTS_Restocking_Charge_UPC__c, APTS_Restocking_Charge_SAP_Nbr__c, APTS_Freight_SAP_Nbr__c,
                                                     APTS_Redistribution__c, APTS_Bill_and_Transmit_RediFreight_as_U__c, APTS_Redi_Freight_SAP_Nbr__c,
                                                     APTS_Fuel_Surcharges__c, APTS_Freight_UPC__c, APTS_Fuel_SAP_Nbr__c,
                                                     APTS_Number_of_Invoice_Copies_Required__c, APTS_Mail_Invoices__c, APTS_Electronic_Billing_Output__c,
                                                     APTS_Special_Billing_EDI_Summary_Etc__c, APTS_Payment_Terms__c, APTS_Payment_Process__c,
                                                     APTS_Delivery_Document__c, APTS_Numberofdeliverydoc_copies_required__c, APTS_Delivery_Notes__c,
                                                     Apttus__Related_Opportunity__c, Apttus__Primary_Contact__c, APTS_Cost_Plus_Programs_is_margin_the_sa__c,
                                                     APTS_cost_plus_pricing_apply_to_non_cont__c
                                                     FROM Apttus__APTS_Agreement__c
                                                     WHERE Id = :agreementId];      
        system.debug(LoggingLevel.Error, '* Parent Agreement: ' + parentAgreement);        
        
        
        if(distiWrapperRecordList.size() > 0){
            for(distiRecordCls distiWr : distiWrapperRecordList){
                if(distiWr.isSelected){
                    Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
                    agreement.Name = distiWr.distiObj.Name;
                    agreement.Apttus__Parent_Agreement__c = agreementId;
                    agreement.RecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Agency Agreement').getRecordTypeId();
                    agreement.APTS_Distributor__c = distiWr.distiObj.id;
                    agreement.Apttus_Approval__Approval_Status__c = 'None';
                    agreement.Apttus__Status__c = 'Request';
                    agreement.Apttus__Status_Category__c = 'Request';
                    agreement.Apttus__Requestor__c = UserInfo.getUserId();
                    
                    //Fields from Parent Agreement
                    agreement.Apttus__Primary_Contact__c = parentAgreement.Apttus__Primary_Contact__c; //CHECK
                    agreement.APTS_Cost_Plus_Programs_is_margin_the_sa__c = parentAgreement.APTS_Cost_Plus_Programs_is_margin_the_sa__c;
                    agreement.APTS_cost_plus_pricing_apply_to_non_cont__c = parentAgreement.APTS_cost_plus_pricing_apply_to_non_cont__c;                    
                    agreement.Apttus__Account__c = parentAgreement.Apttus__Account__c;
                    agreement.APTS_Opportunity_Owner__c = parentAgreement.APTS_Opportunity_Owner__c;
                    agreement.APTS_Corporate_D_B_A_Name__c = parentAgreement.APTS_Corporate_D_B_A_Name__c;
                    agreement.APTS_Customer_Sold_To__c = parentAgreement.APTS_Customer_Sold_To__c;
                    agreement.APTS_Corporate_Account_Director__c = parentAgreement.APTS_Corporate_Account_Director__c;
                    agreement.APTS_Category_Development__c = parentAgreement.APTS_Category_Development__c;
                    agreement.APTS_Customer_Service_Rep__c = parentAgreement.APTS_Customer_Service_Rep__c;
                    agreement.APTS_Customer_Service_Backup__c = parentAgreement.APTS_Customer_Service_Backup__c;
                    agreement.APTS_Credit_Rep__c = parentAgreement.APTS_Credit_Rep__c;
                    agreement.APTS_Integrated_Billing_Rep__c = parentAgreement.APTS_Integrated_Billing_Rep__c;
                    agreement.Apttus__Contract_Start_Date__c = parentAgreement.Apttus__Contract_Start_Date__c;
                    agreement.Apttus__Contract_End_Date__c = parentAgreement.Apttus__Contract_End_Date__c;
                    agreement.APTS_Implementation_Date__c = parentAgreement.APTS_Implementation_Date__c;
                    agreement.APTS_Location_Ship_To__c = parentAgreement.APTS_Location_Ship_To__c;
                    agreement.APTS_Local_Presence_Salesperson_Required__c = parentAgreement.APTS_Local_Presence_Salesperson_Required__c;
                    agreement.APTS_Network_Fee__c = parentAgreement.APTS_Network_Fee__c;
                    agreement.APTS_IT_fee__c = parentAgreement.APTS_IT_fee__c;
                    agreement.APTS_Customer_Incentive_Rebate__c = parentAgreement.APTS_Customer_Incentive_Rebate__c;
                    agreement.APTS_Other_Fee_Describe__c = parentAgreement.APTS_Other_Fee_Describe__c;
                    agreement.APTS_Customer_Negotiated__c = parentAgreement.APTS_Customer_Negotiated__c;
                    agreement.APTS_Mandatory_Manufacturers__c = parentAgreement.APTS_Mandatory_Manufacturers__c;
                    agreement.APTS_Network_Negotiated__c = parentAgreement.APTS_Network_Negotiated__c;
                    agreement.APTS_Common_cost_contracted__c = parentAgreement.APTS_Common_cost_contracted__c;
                    agreement.APTS_Tier_1_Defenition__c = parentAgreement.APTS_Tier_1_Defenition__c;
                    agreement.APTS_Tier_2_Defenition__c = parentAgreement.APTS_Tier_2_Defenition__c;
                    agreement.APTS_Tier_3_Defenition__c = parentAgreement.APTS_Tier_3_Defenition__c;
                    agreement.APTS_Tier_4_Defenition__c = parentAgreement.APTS_Tier_4_Defenition__c;
                    agreement.APTS_Tier_5_Defenition__c = parentAgreement.APTS_Tier_5_Defenition__c;
                    agreement.APTS_Tier_6_Defenition__c = parentAgreement.APTS_Tier_6_Defenition__c;
                    agreement.APTS_Tier_7_Defenition__c = parentAgreement.APTS_Tier_7_Defenition__c;
                    agreement.APTS_Pricing_Basis__c = parentAgreement.APTS_Pricing_Basis__c;
                    agreement.APTS_Other_Pricing_basis__c = parentAgreement.APTS_Other_Pricing_basis__c;
                    agreement.APTS_Pricing_Notes__c = parentAgreement.APTS_Pricing_Notes__c;
                    agreement.APTS_Dispenser_Program__c = parentAgreement.APTS_Dispenser_Program__c;
                    agreement.APTS_Proprietary_Products__c = parentAgreement.APTS_Proprietary_Products__c;
                    agreement.APTS_Dead_Stock__c = parentAgreement.APTS_Dead_Stock__c;
                    agreement.APTS_Slow_Moving_Stock__c = parentAgreement.APTS_Slow_Moving_Stock__c;
                    agreement.APTS_Inventory_Notes__c = parentAgreement.APTS_Inventory_Notes__c;
                    agreement.APTS_PO_Required__c = parentAgreement.APTS_PO_Required__c;
                    agreement.APTS_Order_guides_required_from_Network__c = parentAgreement.APTS_Order_guides_required_from_Network__c;
                    agreement.APTS_Back_order_policy__c = parentAgreement.APTS_Back_order_policy__c;
                    agreement.APTS_CustomerApprovalNecessaryfor_Order__c = parentAgreement.APTS_CustomerApprovalNecessaryfor_Order__c;
                    agreement.APTS_Electronic_Order_Guide_MaintenanceN__c = parentAgreement.APTS_Electronic_Order_Guide_MaintenanceN__c;
                    agreement.APTS_Minimum_Order_Fee__c = parentAgreement.APTS_Minimum_Order_Fee__c;
                    agreement.APTS_Minimum_order_size__c = parentAgreement.APTS_Minimum_order_size__c;
                    agreement.APTS_Minimum_Fee_Amount__c = parentAgreement.APTS_Minimum_Fee_Amount__c;
                    agreement.APTS_Bill_and_Transmit_Mini_order_fee_as__c = parentAgreement.APTS_Bill_and_Transmit_Mini_order_fee_as__c;
                    agreement.APTS_Emergency_Order_Fee_SAP_Nbr__c = parentAgreement.APTS_Emergency_Order_Fee_SAP_Nbr__c;
                    agreement.APTS_Emergency_order_fee_notes__c = parentAgreement.APTS_Emergency_order_fee_notes__c;
                    agreement.APTS_Emergency_Order_Fee_UPC__c = parentAgreement.APTS_Emergency_Order_Fee_UPC__c;
                    agreement.APTS_Order_fee_SAP_Nbr__c = parentAgreement.APTS_Order_fee_SAP_Nbr__c;
                    agreement.APTS_Large_Order_Discount__c = parentAgreement.APTS_Large_Order_Discount__c;
                    agreement.APTS_LOD_order_size__c = parentAgreement.APTS_LOD_order_size__c;
                    agreement.APTS_LOD_Fee_Amount__c = parentAgreement.APTS_LOD_Fee_Amount__c;
                    agreement.APTS_LOD_fee_UPC__c = parentAgreement.APTS_LOD_fee_UPC__c;
                    agreement.APTS_LOD_fee_SAP_Nbr__c = parentAgreement.APTS_LOD_fee_SAP_Nbr__c;
                    agreement.APTS_Restocking_Fee__c = parentAgreement.APTS_Restocking_Fee__c;
                    agreement.APTS_Restocking_Charge_UPC__c = parentAgreement.APTS_Restocking_Charge_UPC__c;
                    agreement.APTS_Restocking_Charge_SAP_Nbr__c = parentAgreement.APTS_Restocking_Charge_SAP_Nbr__c;
                    agreement.APTS_Freight_UPC__c = parentAgreement.APTS_Freight_UPC__c;
                    agreement.APTS_Freight_SAP_Nbr__c = parentAgreement.APTS_Freight_SAP_Nbr__c;
                    agreement.APTS_Redistribution__c = parentAgreement.APTS_Redistribution__c;
                    agreement.APTS_Bill_and_Transmit_RediFreight_as_U__c = parentAgreement.APTS_Bill_and_Transmit_RediFreight_as_U__c;
                    agreement.APTS_Redi_Freight_SAP_Nbr__c = parentAgreement.APTS_Redi_Freight_SAP_Nbr__c;
                    agreement.APTS_Fuel_Surcharges__c = parentAgreement.APTS_Fuel_Surcharges__c;
                    agreement.APTS_Fuel_SAP_Nbr__c = parentAgreement.APTS_Fuel_SAP_Nbr__c;
                    agreement.APTS_Number_of_Invoice_Copies_Required__c = parentAgreement.APTS_Number_of_Invoice_Copies_Required__c;
                    agreement.APTS_Mail_Invoices__c = parentAgreement.APTS_Mail_Invoices__c;
                    agreement.APTS_Electronic_Billing_Output__c = parentAgreement.APTS_Electronic_Billing_Output__c;
                    agreement.APTS_Special_Billing_EDI_Summary_Etc__c = parentAgreement.APTS_Special_Billing_EDI_Summary_Etc__c;
                    agreement.APTS_Payment_Terms__c = parentAgreement.APTS_Payment_Terms__c;
                    agreement.APTS_Payment_Process__c = parentAgreement.APTS_Payment_Process__c;
                    agreement.APTS_Delivery_Document__c = parentAgreement.APTS_Delivery_Document__c;
                    agreement.APTS_Numberofdeliverydoc_copies_required__c = parentAgreement.APTS_Numberofdeliverydoc_copies_required__c;
                    agreement.APTS_Delivery_Notes__c = parentAgreement.APTS_Delivery_Notes__c;
                    agreement.Apttus__Related_Opportunity__c = parentAgreement.Apttus__Related_Opportunity__c;
                    agreement.APTS_Created_From_VF__c = true; //flag
                    insChildAgreement.add(agreement);
                    
                    //remove selected from table
                    distiWrapperRemoveRecords.add(distiWr);
                }
            }//End For Loop
            
            if(insChildAgreement.size() > 0){
                system.debug(LoggingLevel.ERROR, '* Insert Child Agreements: ' + insChildAgreement);
                insert insChildAgreement;
            }
            
            for(distiRecordCls rec : distiWrapperRemoveRecords){
                //remove agreement from vf table
                integer index = distiWrapperRecordList.indexOf(rec);
                distiWrapperRecordList.remove(index);                               
            }

            
        }

        return true;
    }
    
    //cancel : return to agreement
    public PageReference doCancel(){
        try {
            PageReference tempPage = new PageReference('/' + agreementId);           
            tempPage.setRedirect(true);
            //system.debug('*****Save: Agreement Id: ' + agreementID);
			return tempPage;
		}
        catch(Exception ex) {
       	    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        } 
        return null;
    }    
    
    //Wrapper class with checkbox and Distributor__c object
 	public class distiRecordCls{
        public integer index  {get;set;}
  		public boolean isSelected {get;set;}
  		public Distributor__c distiObj {get;set;}
	}    
}