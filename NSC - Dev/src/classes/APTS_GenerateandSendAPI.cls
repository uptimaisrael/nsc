public with sharing class APTS_GenerateandSendAPI {
    
    private static final String docFormat = 'PDF';
    private static final String accessLevel = 'Read/Write';
    private static final Boolean activeTemplate = true;
    
    public static String doGenerate(Id AgreementId) {
        
        Id templateId;
        
        //validate agreement id test
        if(AgreementId == null) {
            system.debug(Logginglevel.ERROR, '* No Agreement sent.');
            return null;
        }        
        
        //get agreement         
        List<Apttus__APTS_Agreement__c> agmts = [SELECT Id, name, Apttus__Status_Category__c, Apttus__Status__c, Apttus__Contract_End_Date__c, RecordType.Name
                                                 FROM Apttus__APTS_Agreement__c 
                                                 WHERE id = :AgreementId LIMIT 1];

        if (agmts != null & !agmts.isEmpty()){
            List<Apttus__APTS_Template__c> templates = [SELECT Id, Name 
                                                        FROM Apttus__APTS_Template__c 
                                                        WHERE Name = :agmts.get(0).RecordType.Name
                                                        AND Apttus__IsActive__c=: activeTemplate LIMIT 1];

            if(templates != null && !templates.isEmpty()){
                templateId = templates.get(0).Id;    
            }
            else {
                system.debug(Logginglevel.ERROR, '* No Template Test_Agreement Found in System.');
                return null;    
            }            

            String apiServerURL = System.Url.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/14.0/' + UserInfo.getOrganizationId();
            String generatedDocId;
            
            if(!Test.isRunningTest()){
                generatedDocId = Apttus.MergeWebService.generateDoc(templateId, agreementId, accessLevel, docFormat, userInfo.getSessionId(), apiServerURL);
            }
            if(generatedDocId != null){
                return generatedDocId;
            }
            else {
                
                return null;     
            }            
        }
        else{
            system.debug(Logginglevel.ERROR, '* No Agreement Found.');
            return null;     
        }        
    }
    
    public static String redirectToEchosign(Id AgreementID, Id documentID){
        
        String attachmentTitle;
        Integer agreementNameLength;
        List<Attachment> atts = null;
        List<echosign_dev1__SIGN_Recipients__c> recipientsList = new List<echosign_dev1__SIGN_Recipients__c>();
        List<Apttus__APTS_Agreement__c> agmts;
        echosign_dev1__SIGN_Agreement__c echosignAgreement = new echosign_dev1__SIGN_Agreement__c();
        
        
        //fetching attachment to populate the agreement name
        if(documentID != null){
            atts = [select id, name, body, parentID from attachment where id =: documentID limit 1];
            if(atts != null && !atts.isEmpty()){
                attachmentTitle = atts.get(0).name;
                agreementNameLength = 80;
            }
        }
        
        if(AgreementID != null){ 
            agmts = [SELECT Id, name, Apttus__Status_Category__c,  Apttus__Status__c, Apttus__Contract_End_Date__c,
                     APTS_Default_Email_Recipients__c FROM Apttus__APTS_Agreement__c WHERE id = :AgreementId];   
        }
        
        //create EchoSignAgreement and pre-populating the message and name
        echosignAgreement.Apttus_Echosign__Apttus_Agreement__c = agmts.get(0).id;
        if(!attachmentTitle.contains('signed')) {             
            Integer index = attachmentTitle.lastindexOf('.');    
            if(index != -1) {
                attachmentTitle = attachmentTitle.substring(0, index);
            } 
            
            Integer len = attachmentTitle.length();
            if (len > agreementNameLength)
                len = agreementNameLength;
            echosignAgreement.Name = attachmentTitle.substring(0, len);
        }
        else{
            echosignAgreement.Name = agmts.get(0).name;
        }
        insert echosignAgreement;
        
        
        
        //creating EchoSignRecipients
        List<String> emails = agmts.get(0).APTS_Default_Email_Recipients__c.split(';');
        integer i = 0;
        for(string s : emails){
            echosign_dev1__SIGN_Recipients__c rec = new echosign_dev1__SIGN_Recipients__c();
            rec.echosign_dev1__Agreement__c = echosignAgreement.Id;
            rec.echosign_dev1__Recipient_Type__c = 'Email';
            rec.echosign_dev1__Email_Address__c = s;
            rec.echosign_dev1__Order_Number__c = i;
            recipientsList.add(rec);    
            i++;
        }
        if(recipientsList.size() > 0){
            system.debug(Logginglevel.ERROR, '* Insert Recipiets: ' + recipientsList);
        	insert recipientsList;    
        }
        
        //Attaching the document to Echosign agreement
        Attachment attToEchoSignAgreement = atts.get(0).clone();
        attToEchoSignAgreement.parentId = echosignAgreement.id;
        insert attToEchoSignAgreement;
        
        // update echosignAgreement;
        echosignAgreement.echosign_dev1__ReturnURL__c = '/'+ AgreementID;
        echosignAgreement.echosign_dev1__Signature_Flow__c = 'Order Entered';
        update echosignAgreement;

        return echosignAgreement.id;
    }
}