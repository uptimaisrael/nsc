trigger Up_AgreementTrigger on Apttus__APTS_Agreement__c (before insert, after insert) {

    Up_AgreementTrigger_Handler handler = new Up_AgreementTrigger_Handler();

    /* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
		system.debug(Logginglevel.ERROR, '*** Up_AgreementTrigger Before Insert ***');      
        handler.OnBeforeInsert_GetContacts(Trigger.new);
    }    
    
    /* After Insert */
    if(Trigger.isInsert && Trigger.isAfter){
        system.debug(Logginglevel.ERROR, '*** Up_AgreementTrigger After Insert ***');
        handler.OnAfterInsert_GenerateAndRedirectDoc(Trigger.new);
        handler.OnAfterInsert_ClonePricingBasis(Trigger.new);
    }    
}